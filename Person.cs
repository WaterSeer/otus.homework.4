﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircularListApp
{
    class Person:IComparable
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double Balance { get; set; }

        /// <summary>
        /// Конструктор класса Person
        /// </summary>
        /// <param name="firstName">Имя</param>
        /// <param name="lastNmae">Фамилия</param>
        /// <param name="birthdate">Дата Рождения</param>
        private Person() { }
        /// <summary>
        /// Конструктор класса Person c параметрами
        /// </summary>
        /// <param name="firstName">Имя</param>
        /// <param name="lastName">Фамилия</param>
        /// <param name="balance">Баланс банковского счёта</param>
        public Person(string firstName, string lastName, double balance)
        {            
            FirstName = firstName;
            LastName = lastName;
            Balance = balance;
        }
        /// <summary>
        /// Возвращает строку с данными сотрудника
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"Name: {FirstName} {LastName} Balance: {Balance}";

        public override bool Equals(object obj) => obj.ToString() == ToString();
        public override int GetHashCode() => ToString().GetHashCode();


        /// <summary>
        /// Сравнение двух карточек сотрудника
        /// </summary>
        /// <param name="p1">Карточка сторудника</param>
        /// <param name="p2">Карточка сторудника</param>
        /// <returns></returns>
        public static bool operator ==(Person p1, Person p2) => p1.Equals(p2);

        /// <summary>
        /// Сравнение двух карточек сотрудников
        /// </summary>
        /// <param name="p1">Карточка сторудника</param>
        /// <param name="p2">Карточка сторудника</param>
        /// <returns></returns>
        public static bool operator !=(Person p1, Person p2) => !p1.Equals(p2);

        
        /// <summary>
        /// Метод списывает со счёта некоторое количество денег
        /// </summary>
        /// <param name="amount"></param>
        public void Debit(double amount)
        {
            if (amount > Balance)
            {
                throw new ArgumentOutOfRangeException("amount");
            }
            if (amount < 0)
            {
                throw new ArgumentOutOfRangeException("amount");
            }
            Balance -= amount;
        }
        /// <summary>
        /// Метод увеличивает величину счёта на указаное количество
        /// </summary>
        /// <param name="amount"></param>
        public void Credit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentOutOfRangeException("amount");
            }
            Balance += amount;
        }

        public int CompareTo(object obj)
        {
            Person p = obj as Person;
            if (p != null)
                return this.LastName.CompareTo(p.LastName);
            else
                throw new Exception("Невозможно сравнить два объекта");
        }
    }
}
