﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircularListApp
{
    class CircularLinkedList<T> : IEnumerable<T>, IAlgorithm<T> where T: IComparable
    {
        Node<T> head;        
        int count;
        
        public void Add(T data)
        {
            Node<T> node = new Node<T>(data);
            if (head == null)
            {
                head = node;
                head.Next = node;
                head.Previous = node;
            }
            else
            {
                node.Previous = head.Previous;
                node.Next = head;
                head.Previous.Next = node;
                head.Previous = node;
            }
            count++;
        }   
        
        public bool Remove(T data)
        {
            Node<T> current = head;

            Node<T> removedItem = null;
            if (count == 0) return false;
            
            do
            {
                if (current.Data.Equals(data))
                {
                    removedItem = current;
                    break;
                }
                current = current.Next;
            }
            while (current != head);

            if (removedItem != null)
            {                
                if (count == 1)
                    head = null;
                else
                {                    
                    if (removedItem == head)
                    {
                        head = head.Next;
                    }
                    removedItem.Previous.Next = removedItem.Next;
                    removedItem.Next.Previous = removedItem.Previous;
                }
                count--;
                return true;
            }
            return false;
        }
        public int Count { get { return count; } }
        public bool IsEmpty { get { return count == 0; } }

        public void Clear()
        {
            head = null;            
            count = 0;
        }

        public bool Contains(T data)
        {
            Node<T> current = head;
            if (current == null)
                return false;
            do
            {
                if (current.Data.Equals(data))
                    return true;
                current = current.Next;
            }
            while (current != head);
            return false;
        }
               

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            Node<T> current = head;
            do
            {
                if (current != null)
                {
                    yield return current.Data;
                    current = current.Next;
                }
            }
            while (current != head);
        }

        public int BinarySearch(T Data)
        {
            Node<T> current;
            int left = 0;
            int right = Count - 1;
            int searchNode = -1;
            while(left<=right)
            {                
                int middle=(right+left)/ 2;
                current = head;
                for (int j = 0; j < middle; j++)
                {
                    current = current.Next;
                }
                if(current.Data.CompareTo(Data)==0)
                {
                    searchNode = middle;
                    break;
                }
                if (current.Data.CompareTo(Data) < 0)
                    left = middle + 1;
                else
                    right = middle - 1;
            }
            return searchNode; 
        }

        public void Sort()
        {
            if (IsEmpty)
                return;
            Node<T> current = head;
            Node<T> buffer = null;
            do
            {
                do
                {                    
                    if (current.Data.CompareTo(current.Next.Data) < 0)
                    {                        
                        buffer = current.Next;
                        current = current.Next;
                        current = buffer;
                    }
                    current = current.Next;
                }
                while (current != head);
            }
            while (current != head);
            return;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in this)
            {
                sb.Append(item.ToString()); 
            }
            return sb.ToString();
        }
    } 
}
