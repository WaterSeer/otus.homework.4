﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircularListApp
{
    interface IAlgorithm<T>
    {        
        void Sort();
        int BinarySearch(T Data);
    }
}
