﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircularListApp
{
    class IntDictionary:IComparable
    {
        public string Name { get; set; }
        public int Score { get; set; }

        public IntDictionary(string name, int score)
        {
            Name = name;
            Score = score;
        }

        public int CompareTo(object obj)
        {
            IntDictionary i = obj as IntDictionary;
            return Score.CompareTo(i.Score);
        }
        public override string ToString()
        {
            return "Name: " + Name + ", " + Score.ToString();
        }
    }
}
