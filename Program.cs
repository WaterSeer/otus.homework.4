﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircularListApp
{
    class Program
    {
        static void Main(string[] args)
        {
            CircularLinkedList<IntDictionary> myCLL = new CircularLinkedList<IntDictionary>();

            IntDictionary intDictionary1 = new IntDictionary("first", 1);
            IntDictionary intDictionary2 = new IntDictionary("fourth", 4);
            IntDictionary intDictionary3 = new IntDictionary("thrith", 3);
            IntDictionary intDictionary4 = new IntDictionary("second", 2);

            myCLL.Add(intDictionary1);
            myCLL.Add(intDictionary2);
            myCLL.Add(intDictionary3);
            myCLL.Add(intDictionary4);

            foreach (var item in myCLL)
            {
                Console.WriteLine(item.ToString());
            }
            Console.WriteLine();

            myCLL.Remove(intDictionary3);
            foreach (var item in myCLL)
            {
                Console.WriteLine(item.ToString());
            }            
            Console.ReadLine();
        }
    }
}
